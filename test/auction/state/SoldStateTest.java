/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auction.state;

import auction.entity.Lot;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Olga
 */
public class SoldStateTest {
   
    /**
     * Test of raisePrice method, of class SoldState.
     */
    @Test
    public void testRaisePrice() {
        System.out.println("raisePrice");
        Lot conext = new Lot("name", 10);
        double price = 10.0;
        int percent = 20;
        String buyerName = "";
        SoldState instance = new SoldState();
        double expResult = 12.0;
        double result = instance.raisePrice(conext, price, percent, buyerName);
        assertEquals(expResult, result, 0.0);
    }
}
