/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auction.state;

import auction.entity.Lot;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Olga
 */
public class ForSaleStateTest {
    
    /**
     * Test of raisePrice method, of class ForSaleState.
     */
    @Test
    public void testRaisePrice() {
        System.out.println("raisePrice");
        Lot conext = new Lot("name", 100);
        double price = 100.0;
        int percent = 20;
        String buyerName = "";
        ForSaleState instance = new ForSaleState();
        double expResult = 120.0;
        double result = instance.raisePrice(conext, price, percent, buyerName);
        assertEquals(expResult, result, 0.0);
    }
}
