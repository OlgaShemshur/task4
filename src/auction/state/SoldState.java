/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auction.state;

import auction.entity.Lot;
import static auction.state.ForSaleState.log;

/**
 *
 * @author Olga
 */
public class SoldState implements SellStatus {

    @Override
    public double raisePrice(Lot conext, double price, int percent, String buyerName) {
        price = price * (1.0 + (double)percent/100);
        conext.setBuyerPreview(conext.getBuyerName());
        conext.setBuyerName(buyerName);
        conext.setPrice(price);
        return price;
    }

    @Override
    public void print(Lot conext) {
        String message = "\r\nSOLD\r\n__________\r\nLot: " + conext.getName() + "\r\nBuyer: " + conext.getBuyerName();
        message += "\r\nPrice: " + String.format("%.2f", conext.getPrice());
        log.info(message);
    }    

    @Override
    public double backPrice(Lot conext, double price, int percent) {
        price = price + (1.0 + (double)percent/100);
        conext.setPrice(price);
        conext.setBuyerName(conext.getBuyerPreview());
        return price;
    }

    @Override
    public int raisePercent(Lot conext, int percent) {
        return 0;
    }

    @Override
    public int backPercent(Lot conext) {
         return 0;
    }

    @Override
    public int getStateCode() {
        return 1;
    }
}
