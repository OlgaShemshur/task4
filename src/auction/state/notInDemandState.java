/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auction.state;

import auction.entity.Lot;
import static auction.state.ForSaleState.log;

/**
 *
 * @author Olga
 */
public class notInDemandState implements SellStatus {

    @Override
    public int raisePercent(Lot conext, int percent) {
       return 0;
    }

    @Override
    public int backPercent(Lot conext) {
        return 0;
    }

    @Override
    public double raisePrice(Lot conext, double price, int percent, String buyerName) {
       return 0;
    }
    
    @Override
    public void print(Lot conext) {
        String message = "\r\nNOT SOLD\r\n__________\r\nLot: " + conext.getName();
        log.info(message);
    }
    
    @Override
    public double backPrice(Lot conext, double price, int percent) {
        return 0;
    }

    @Override
    public int getStateCode() {
        return 2;
    }    
}
