/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auction.state;

import auction.entity.Lot;

/**
 *
 * @author Olga
 */
public interface SellStatus {
    public int raisePercent(Lot conext, int percent);
    public int backPercent(Lot conext);
    public double raisePrice(Lot conext, double price, int percent, String buyerName);
    public void print(Lot conext);
    public double backPrice(Lot conext, double price, int percent);
    public int getStateCode();
}
 