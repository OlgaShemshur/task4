/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auction.state;

import auction.entity.Lot;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 *
 * @author Olga
 */
public class ForSaleState implements SellStatus {

    static{
        new DOMConfigurator().doConfigure("src\\auction\\resource\\log4j.xml", LogManager.getLoggerRepository());
    }
    static Logger log = Logger.getLogger("log");
    
    @Override
    public double raisePrice(Lot conext, double price, int percent, String buyerName) {
        price = price *(1.0 + (double)percent/100);
        conext.setBuyerPreview(conext.getBuyerName());
        conext.setBuyerName(buyerName);
        conext.setPrice(price);
        return price;
    }

    @Override
    public void print(Lot conext) {
        String message = "\r\nFOR SELL\r\n__________\r\nLot: " + conext.getName() + "\r\nBuyer: " + conext.getBuyerName();
        message += "\r\nPrice: " + conext.getPrice() + "\r\nPercent: " + conext.getPercent() + "\r\n";
        log.info(message);
    }

    @Override
    public double backPrice(Lot conext, double price, int percent) {
        price = price/(1.0 + (double)percent/100);
        conext.setBuyerName(conext.getBuyerPreview());
        conext.setPrice(price);
        return price;
    }

    @Override
    public int raisePercent(Lot conext, int percent) {
        conext.setPreviewPercent(conext.getPercent());
        conext.setPercent(percent);
        return conext.getPercent();
    }

    @Override
    public int backPercent(Lot conext) {
        conext.setPercent(conext.getPreviewPercent());
        return conext.getPercent();
    }    

    @Override
    public int getStateCode() {
        return 0;
    }
}
