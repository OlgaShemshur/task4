/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auction.helper;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 *
 * @author Olga
 */
public class FileWork implements Runnable {
    private boolean ready = false;
    private final String fileName;
    private String action;
    private String data;
    private int index;
    private int noteCount;
    private  ArrayList<String> allData = new ArrayList<>();
    private  ArrayList<String> escape = new ArrayList<>();
    private Lock lock;
    static{
        new DOMConfigurator().doConfigure("src\\auction\\resource\\log4j.xml", LogManager.getLoggerRepository());
    }
    static Logger console = Logger.getLogger("console");
    
    public FileWork(String fileName, String action, int index) {
        this.fileName = fileName;
        this.action = action;
        data = null;
        this.index = index;
        escape.add("\n");
        escape.add("|");
        escape.add("");
        escape.add("\r");
        initNoteCount();
        this.lock = new ReentrantLock();
    }
    
    public FileWork(String fileName, String action, String data) {
        this.fileName = fileName;
        this.action = action;
        this.data = data;
        index = 0;
        initNoteCount();
    }

    public void setAction(String action) {
        this.action = action;
        index = 0;
        ready = false;
        if (action.equals("read")) {
            allData.clear();
        }
    }
   
    public boolean isReady() {
        return ready;
    }
    
    public void setData(String data) {
        this.data = data;
    }
    
    public void setIndex(int index) {
        this.index = index;
    }
    
    public int getNoteCount() {
        return this.noteCount;
    }
    
    public String getAllDataElement(int i) {
        return this.allData.get(i);
    }
    
    public ArrayList<String> getAllData() {
        return this.allData;
    }
    
    public String getData() {
        return this.data;
    }
    
    private void initNoteCount() {
        int count = 0;
        try(FileReader reader = new FileReader(fileName))
        {
            int c;
            int i = 0;
            while((c=reader.read())!=-1){ 
                char k = (char)c;
                if(k=='\n') {
                    count++;
                }
            } 
        }
        catch(IOException ex){
           console.error(ex);
        } 
        finally {
            this.noteCount = count;
        }
    }
    
    @Override
    public void run() {
        switch (action) {
            case "write":
        {
            try {
                write();
            } catch (InterruptedException ex) {
                java.util.logging.Logger.getLogger(FileWork.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
                break;
            case "read":
        {
            try {
                data = read();
            } catch (InterruptedException ex) {
                java.util.logging.Logger.getLogger(FileWork.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        ArrayList<String> tmp = new ArrayList<>();
                tmp.addAll(Arrays.asList(data.split("\\|")));
                for(String s: tmp) {
                    allData.addAll(Arrays.asList(s.split("\\r\\n")));
                }
                allData.removeAll(escape);
                break;
        }
        ready = true;
    }  
    
    private boolean write() throws InterruptedException {
        lock.lock();
        try(FileWriter writer = new FileWriter(fileName))
        {
            writer.append(data);
            writer.append("\n");
            writer.flush();
            writer.close();
        }
        catch(IOException ex){
           console.error(ex);
           return false;
        } 
        finally {
            lock.unlock();
        }
        return true;
    }
    
    private String read() throws InterruptedException {
        lock.lock();
        String s = "";
        try(FileReader reader = new FileReader(fileName))
        {
            int c;
            int i = 0;
            while((c=reader.read())!=-1){ 
                char k = (char)c;
                s += k;
            } 
            reader.close();
        }
        catch(IOException ex){
           console.error(ex);
        } 
        finally {
            lock.unlock();
        }
    return s;
    }     
}
