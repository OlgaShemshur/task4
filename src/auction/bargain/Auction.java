/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auction.bargain;

import auction.entity.Buyer;
import auction.entity.Lot;
import static auction.helper.ActionWithFile.READ;
import static auction.helper.FileName.*;
import auction.helper.FileWork;
import java.util.ArrayList;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 *
 * @author Olga
 */
public class Auction {

    static{
        new DOMConfigurator().doConfigure("src\\auction\\resource\\log4j.xml", LogManager.getLoggerRepository());
    }
    static Logger console = Logger.getLogger("console");
    
    public static void main(String[] args) throws InterruptedException {
        FileWork lotsFile = new FileWork(LOTS, READ, 0);
        FileWork buyerFile = new FileWork(BUYERS, READ, 0);
        Thread t = new Thread(lotsFile);
        Thread t2 = new Thread(buyerFile);
        t.start();
        t2.start();
        t.join();
        t2.join();
        ArrayList<Buyer> buyers = new ArrayList<>();
        for(int i=0; i< buyerFile.getAllData().size(); i+=2) {
            buyers.add(new Buyer(buyerFile.getAllData().get(i), Integer.valueOf(buyerFile.getAllData().get(i+1))));
        }
        ArrayList<Lot> lots = new ArrayList<>();
        for(int i=0; i< lotsFile.getAllData().size(); i+=2) {
            lots.add(new Lot(lotsFile.getAllData().get(i), Integer.valueOf(lotsFile.getAllData().get(i+1))));
        }
        new Thread(new Bargain(buyers, lots)).start();
    }         
}
