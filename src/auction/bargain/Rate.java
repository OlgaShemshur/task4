/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auction.bargain;

import auction.entity.Buyer;
import auction.entity.Lot;
import auction.state.SoldState;

/**
 *
 * @author Olga
 */
public class Rate implements Runnable {

    private Buyer buyer;
    private Lot lot;
    private int percent;
    
    public Rate(int percent, Buyer buyer, Lot lot) {
        this.buyer = buyer;
        this.lot = lot;
    }
    
    @Override
    public void run() {
        if(lot.getStateCode()==0) {
            if(buyer.MakeRate(lot, percent+lot.getPercent())) {
                lot.setState(new SoldState());
                Bargain.addSoldLots(lot);
                Bargain.changeSizeOfLotsArray();
                Bargain.changeSizeOfSoldLotsArray();
            }
        }
    }    
}
