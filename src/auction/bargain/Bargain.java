/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auction.bargain;

import auction.entity.Buyer;
import auction.entity.Lot;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 *
 * @author Olga
 */
public class Bargain implements Runnable {

    private static int sizeOfLotsArray;
    private static int sizeOfInActiveLotsArray;
    private static int sizeOfSoldLotsArray;
    private static Lock lock;
    private ArrayList<Buyer> buyers;
    private static ArrayList<Lot> lots;
    private static ArrayList<Lot> inActiveLots = new ArrayList<>();
    private static ArrayList<Lot> soldLots = new ArrayList<>();
    static{
        new DOMConfigurator().doConfigure("src\\auction\\resource\\log4j.xml", LogManager.getLoggerRepository());
    }
    static Logger log = Logger.getLogger("log");
    
    public Bargain(ArrayList<Buyer> buyers, ArrayList<Lot> lots) {
        sizeOfLotsArray = lots.size();
        this.buyers = buyers;
        this.lots = lots;
        this.lock = new ReentrantLock();
    }
            
    @Override
    public void run() {
        for (Lot tmp: lots) {
            Double triesRandom = Math.random()*3;
            Thread t = new Thread(new LotBlocking(tmp));
            t.start();
            while(triesRandom.intValue()!=0 && tmp.getStateCode()!=2) {
                Double buyerRandom = Math.random()*buyers.size();
                Double percentRandom = Math.random()*10+15;
                try{
                    Rate rate = new Rate(percentRandom.intValue(), buyers.get(buyerRandom.intValue()), tmp);
                    rate.run();
                }
                catch (IndexOutOfBoundsException ex) {
                    triesRandom--;
                    continue;
                } 
                triesRandom--;
            }
            try {
                t.join();
            } catch (InterruptedException ex) {
                java.util.logging.Logger.getLogger(Bargain.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        for(Lot tmp: soldLots) {
            tmp.print();
        }
        for(Lot tmp: inActiveLots) {
            tmp.print();
        }
    }
    
    public static void changeSizeOfLotsArray() {
        lock.lock();
        sizeOfLotsArray = 0;
        try{
            for(Lot tmp: lots) {
            if(tmp.getStateCode()==0)
                sizeOfLotsArray++;
        }
        }
        finally{
            lock.unlock();
        }
    }
    
    public static ArrayList<Lot> getLotsArray() {
        lock.lock();
        try{
            return lots;
        }
        finally{
            lock.unlock();
        }
    }
    
    public static int getsizeOfLotsArray() {
        return sizeOfLotsArray;
    }
    
    public static void changeSizeOfInActiveLotsArray() {
        lock.lock();
        try{
            sizeOfInActiveLotsArray++;
        }
        finally{
            lock.unlock();
        }
    }
    
    public static void changeSizeOfSoldLotsArray() {
        lock.lock();
        try{
            sizeOfSoldLotsArray++;
        }
        finally{
            lock.unlock();
        }
    }
    
    public static void deleteLots(Lot tmp) {
        lock.lock();
        try{
            lots.remove(tmp);
        }
        finally{
            lock.unlock();
        }
    }
    
    public static void addSoldLots(Lot tmp) {
        lock.lock();
        try{
            soldLots.add(tmp);
        }
        finally{
            lock.unlock();
        }
    }
    
    public static void addInActiveLots(Lot tmp) {
        lock.lock();
        try{
            inActiveLots.add(tmp);
        }
        finally{
            lock.unlock();
        }
    }
}
