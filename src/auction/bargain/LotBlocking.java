/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auction.bargain;

import auction.entity.Lot;
import auction.state.notInDemandState;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;

/**
 *
 * @author Olga
 */
public class LotBlocking implements Runnable {

    private Lot lot;
    private Lock lock;
    Condition condition;
     static{
        new DOMConfigurator().doConfigure("src\\auction\\resource\\log4j.xml", LogManager.getLoggerRepository());
    }
    static org.apache.log4j.Logger console = org.apache.log4j.Logger.getLogger("console");
    
    public LotBlocking(Lot lot) {
        this.lot = lot;
        lock = new ReentrantLock();
        condition = lock.newCondition();
    }
    
    @Override
    public void run() {
        try {
            while(!waitTime()) {
                condition.await();
            }
            if(lot.getStateCode() != 1) {
                disable();
            }
        } catch (InterruptedException ex) {            
            console.error(ex);
        }
    }    
    
    private boolean waitTime() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(1000);
        return true;
    }
    
    private void disable() {
        lot.setState(new notInDemandState());
        Bargain.addInActiveLots(lot);
        Bargain.changeSizeOfLotsArray();
        Bargain.changeSizeOfInActiveLotsArray();
    }
}
