/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auction.entity;

import java.util.ArrayList;

/**
 *
 * @author Olga
 */
public class Buyer {
    private double money;
    private String name;
    private ArrayList<Lot> lots = new ArrayList<>();
    
    public Buyer(String name, double money) {
        this.name = name;
        this.money = money;
    }
    
    public boolean MakeRate(Lot lot, int percent) {
        if(lot.raisePrice(name, percent)>money) {
            lot.backPrice();
            return false;
        }
        else return true;
    }
    
}
