/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auction.entity;

import auction.bargain.LotBlocking;
import auction.state.ForSaleState;
import auction.state.SellStatus;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Olga
 */
public class Lot {
    private final String name;
    private String byerName;
    private String byerPreview;
    private double price;
    private int percent;
    private int percentPreview;
    private SellStatus satus;
    
    public Lot(String name, double price) {
        setState(new ForSaleState());
        byerPreview = null;
        percent = 15;
        percentPreview = 15;
        this.name = name;
        this.price = price;
    }
    
    public void setState(final SellStatus newState) {
        satus = newState;
    }
    
    public double raisePrice(String buyerName, int percent) {
      price = satus.raisePrice(this, price, percent, buyerName);
      raisePercent(this, percent);
      return price;
    }

    public double backPrice() {
      price = satus.backPrice(this, price, percent);
      return price;
    }
    
    public int raisePercent(Lot conext, int percent) {
        percent = satus.raisePercent(this, percent);
        return percent;
    }
    
    public int backPercent(Lot conext) {
        percent = satus.backPercent(this);
        return percent;
    }
    
    public int getStateCode() {
        return satus.getStateCode();
    }
    
    public void print() {
        satus.print(this);
    } 
    
    public void setBuyerName(String byerName) {
        this.byerName = byerName;
    }
    
    public void setBuyerPreview(String byerPreview) {
        this.byerPreview = byerPreview;
    }
    
    public void setPercent(int percent) {
        this.percent = percent;
    }
    
    public void setPrice(double price) {
        this.price =  price;
    }
    
    public void setPreviewPercent(int percent) {
        this.percentPreview = percent;    
    }
    
    public int getPreviewPercent() {
        return percentPreview;
    }
    
    public String getBuyerName() {
        return byerName;
    }
    
    public String getBuyerPreview() {
        return byerPreview;
    }
    
    public int getPercent() {
        return percent;
    }
    
    public double getPrice() {
        return price;
    }
    
    public String getName() {
        return name;
    }
    
    private boolean waitTime() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(1000);
        return true;
    }
}
